#ifndef _LISTA_H
#define _LISTA_H
#include <stdlib.h>
//#include <lista.h>

typedef struct ElementoListaTDA{
	int dato;
	ElemendoListaTDA *prev;
	ElementoListaTDA *next;
					//Definir los campos del elemento de lista
} ElementoLista;

/**
*Definci�n de la estructura de lista
*/

typedef struct ListaEnlazadaTDA{
	ElemElemendoListaTDA *inicio;
	ElemendoListaTDA *fin;
					//Definir la lista propiamente
}ListaEnlazada;

/*Definici�n de funciones sobre la lista*/
extern int Lista_Inicializar(ListaEnlazada *Lista){
	Lista->inicio=NULL;
	Lista->fin=NULL;	
return 0

};		//0 en exito, -1 en error


extern int Lista_Vacia(ListaEnlazada *Lista){
	if(Lista->Inicio == NULL) return TRUE;
	else return FALSE;
	
};			//TRUE si esta vacia, FALSE si no
extern int Lista_Conteo(ListaEnlazada *Lista){
	int c=0;
	ElementoLista * temp= malloc(sizeof(ELementoLista));
	temp=Lista->inicio;
	While(temp->next != Lista->fin){
		temp=temp->next;
		c++;
	}
	return c;
	free(temp);
};			//Cantidad de elementos en la lista

extern ElementoLista *Lista_Buscar(ListaEnlazada *Lista, int data){
	ElementoLista * temp= (ElementoLista *)malloc(sizeof(ELementoLista));
	temp=Lista->inicio;
	While(temp->next != Lista->fin){
		if(temp->dato = data) printf("SI/n") ;
		else printf("NO/n");
		temp=temp->next;								
	}
	return temp;
	free(temp);
};

/*Funciones de modificacion de la lista*/
extern int Lista_InsertarFin(ListaEnlazada *Lista,int data){
	ElementoLista *nuevo= (ElementoLista *)malloc(sizeof(ELementoLista));
	nuevo->dato= data;
	if(Lista->final==NULL){
		Lista->fin=nuevo;
		Lista->inicio= Lista->fin;
		(Lista->fin)->next=Lista->inicio;
		
		(Lista->fin)->prev=inicio;
	}else{
		ElementoLista *aux= (ElementoLista *)malloc(sizeof(ELementoLista));		
		aux=Lista->final;
		aux->next=nuevo;
		nuevo->prev=aux;
		Lista->fin=nuevo;
		
	}
	free(nuevo);
	return 0;	
	
};			//0 en exito, -1 en error
extern int Lista_InsertarInicio(ListaEnlazada *Lista,int data){
	ElementoLista *nuevo= (ElementoLista *)malloc(sizeof(ELementoLista));
	nuevo->dato= data;
	if(Lista->inicio==NULL){
		Lista->inicio=nuevo;
		Lista->fin= Lista->inicio;
		(Lista->inicio)->next=Lista->fin;
		
		(Lista->fin)->prev=inicio;
	}else{
		ElementoLista *aux= (ElementoLista *)malloc(sizeof(ELementoLista));		
		aux=Lista->inicio;
		nuevo->next=aux;
		aux->prev=nuevo;	
		Lista->inicio=nuevo;
		
	}
	free(nuevo);
	
	return 0;	

		
	
	};		//0 en exito, -1 en error
extern void Lista_Sacar(ListaEnlazada *Lista,int data){
	ElementoListaTDA * temp= (ElementoLista *)malloc(sizeof(ELementoListaTDA));
	temp=Lista->inicio;
	While(temp->next != Lista->fin){
		if(temp->dato = data) return temp ;
		else printf("No se encuentra en la lista");
		temp=temp->next;								
	}
	free(temp);
};				//saca un elemento de la lista
extern void Lista_SacarTodos(ListaEnlazada *Lista){
	ElementoListaTDA * temp= (ElementoListaTDA *)malloc(sizeof(ELementoListaTDA));
	temp=Lista->inicio;
	While(temp->next != Lista->fin){
		
	}

};
extern int Lista_InsertarDespues(/* �arguementos? */);	//0 en exito, -1 en error. Inserta un objeto despues de un elemento particular de la lista
extern int Lista_InsertarAntes(/* �arguementos? */);	//0 en exito, -1 en error. Inserta un objeto antes de un elemento particular de la lista


/*Funciones que devuelven un elemento particular de la lista*/
extern ElementoLista *Lista_Primero(/* �arguementos? */);		//NULL en error
extern ElementoLista *Lista_Ultimo(/* �arguementos? */);		//NULL en error
extern ElementoLista *Lista_Siguiente(/* �arguementos? */);	//NULL en error. Obtiene elemento despues de un elemento particular
extern ElementoLista *Lista_Anterior(/* �arguementos? */);	//NULL en error. Obtiene elemento antes de un elemento particular


#endif
